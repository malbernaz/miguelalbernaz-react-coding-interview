import React from 'react';
import { Form, Input, Button } from 'antd';

type Field = {
  name: string;
  value: any;
};

type Props = {
  fields: Field[];
  onSubmit: (values: any) => void;
  onCancel: () => void;
};

const PersonEditForm: React.FC<Props> = ({ fields, onSubmit, onCancel }) => {
  const handleSubmit = (values: any) => {
    onSubmit(values);
  };

  const initialValues = fields.reduce(
    (v, f) => ({
      ...v,
      [f.name]: f.value ?? '',
    }),
    {}
  );

  return (
    <Form onFinish={handleSubmit} initialValues={initialValues}>
      {fields.map((f) => (
        <Form.Item key={f.name} label={f.name} name={f.name}>
          <Input placeholder={f.name} />
        </Form.Item>
      ))}
      <Form.Item>
        <Button htmlType="submit">submit</Button>
      </Form.Item>
      <Button onClick={onCancel}>cancel</Button>
    </Form>
  );
};

export default PersonEditForm;
